
% TRF_CC_master. 
% This script is paired with the 'TRF_CC_algorithm' script. This script 
% sets the variables that will be used in the TRF_CC_algorithm script.

% A description of the requirements and settings are given below.

%%%%%%%%%%%%%%%%%%%%%%%%%%  
% Requirements:
%%%%%%%%%%%%%%%%%%%%%%%%%%

% Required folders:
    % out files for all subjects.
    %       - reduced out files for all subjects make things a lot faster.
    %       This can be done using the script: 'shorten_out_files.mat'

% Required files:
    %   'experimentalInformation.mat'.
    %       - This contains the speech stream the respective subjects were
    %       attending to.
    
% Required functions:
    %   cepstrum
    %   Generate_Reconstruction_Filters
    %   Reconstruct_Stimulus
    %   LagGenFrames


%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings:
%%%%%%%%%%%%%%%%%%%%%%%%%

% CHANGING:
% ---------
% Settings that change in each loop of the script.

%   - Subject
%   - Frame Length
%   - Overlap (not currently working non-zero overlaps)
%   - Set of channels used
%   - Number of cepstral coefficients used

% FIXED:
% ---------
% Settings that are defined in the script at some point, but which, in 
% theory could be changed or modified.

%   - Preprocessing:
%       - Upsampling
%       - Filtering no/yes (e.g. 1-8Hz)
%       - Detrending

%   - Type of spectral estimation used:
%       - FFT with no filtering (zero-padded)
%       - FFT with filtering (e.g. Hamming, Bartlett) (zero-padded)
%       - Periodogram
%           - standard
%           - with Hanning
%       - Welch method
%       - Multitapering
%           - standard
%           - Maria's method

%   - Lags:
%       - The method of calculating the lags is currently fixed. The number
%       of lags varies depending on the frame length selected. The lags are
%       set so they cover the time period 0:400ms which is the time period
%       in the EEG over which the speech signal has been shown to have a
%       presence.

%   - TRF method:
%       - We are now using the backward TRF, but we can also try the
%       forward TRF.

%   - Features:
%       - Now we are only using cepstral coefficients. We may be able to
%       include other features, e.g. the first and second derivatives of
%       the cepstral coefficients.

%   - Evaluation methods:
%       - We are now using the NMSE and Pearson's Rho to compare the
%       differences between the attended and unattended cepstral
%       coefficients. We include the mean and the variance of these.

%   - Time period over which the prediction is made:
%       - Currently we are using the full minute of test data to calculate
%       the NMSE and Rho values.
%       - This could be reduced to shorter time lengths, i.e. 20s to check
%       performance.

%   - Trials:
%       - All 30 trials are included (each subject underwent 30 test trials
%       respectively).

%%
close, clear, clc
tic

% Set spectral esimation method:
spectralEstimationMethod = 'welch';

% Set directory the subject data will be called from:

run set_directory_paths

% Select the options for testing:

%{
subjects = {'AP'; 'AS'; 'BM'; 'BR'; 'BWM'; 'CB'; 'CM'; 'CN'; 'DC'; ...
'DCM'; 'DM'; 'DW'; 'EK'; 'EL'; 'FW'; 'GB'; 'GC'; 'GG'; 'GK'; 'GL'; ...
'GM'; 'HN'; 'IK'; 'JB'; 'JS'; 'KM'; 'MK'; 'NC'; 'RC'; 'RoC'; 'RW'; ...
'SD'; 'SP'; 'ToD'};

subjects = {'AP'; 'AS'; 'BM'; 'BR'; 'BWM'; 'CB'; 'CM'; 'CN'; 'DC'; ...
'DM'; 'DW'; 'EK'; 'EL'; 'FW'; 'GB'; 'GC'; 'GG'; 'GK'; 'GL'; ...
'HN'; 'JB'; 'JS'; 'KM'; 'MK'; 'NC'; 'RC'; 'RoC'; 'RW'; ...
'SD'; 'SP'; 'ToD'};
% missing: 'DCM',  'GM', 'IK'
%}

subjects = {'AP'};

frameLengths = [25];
overlaps = [0];
%channelSets{1,1} = [1, 19, 23, 54, 58, 81, 85,  114, 119];
%channelSets{1,2} = {'NineSet'};
channelSets{1,1} = [58,119]; %three channels: centre and two ears
channelSets{1,2} = {'EarSet_2'};
%channelSets{1,1} = 1:128;
%channelSets{1,2} = {'FullSet'};
cepCoeffs = [8];

% Here we find the number of each respective option. These are used in the
% loop below.
nSubjects = length(subjects);
nFrameLengths = length(frameLengths);
nOverlaps = length(overlaps);
nChannelSets = size(channelSets,1);
nCepCoeffs = length(cepCoeffs);


% Here we create f, a struct that will store the most useful information:
f = {};
f{1} = 'subjects';
f{2} = 'frameLength';
f{3} = 'overlap';
f{4} = 'channels';
f{5} = 'cepCoeffs';
f{6} = 'nmseClass';
f{7} = 'nmsemeanattended';
f{8} = 'nmseStdAttended';
f{9} = 'nmseMeanUnattended';
f{10} = 'nmseStdUnattended';
f{11} = 'rhoClass';
f{12} = 'rhoMeanAttended';
f{13} = 'rhoStdAttended';
f{14} = 'rhoMeanUnattended';
f{15} = 'rhoStdUnattended';

% Here we calculate the number of different combinations of variables. This
% is needed to calculate the number times we will run through in the loop
nCombinations = length(subjects)*length(frameLengths)*length(overlaps)*...
    size(channelSets,1)*length(cepCoeffs);

f{2,1} = {};
dataStore = struct(f{:});

% record of where file already existed
fileAlreadyExists = [];

% record of when reduced out file doesnt exist
runFails = [];


for ii = 1:nSubjects
    % Runs through all subjects
    
    thisSubject = subjects{ii}
    DataPath = [directory '\' thisSubject '\'];
    
    if exist([DataPath 'out_' thisSubject  '_reduced.mat'], 'file') == 2
        % If this subject has a reduced out file:
        
        load([DataPath 'out_' thisSubject '_reduced.mat']);
        
        for jj = 1:nFrameLengths
            
            thisFrameLength = frameLengths(jj);
           
            for kk = 1:nOverlaps
                % Consider different overlaps
                
                thisOverlap = overlaps(kk);
                
                for ll = 1:nChannelSets
                    % Consider different channel sets
                    
                    thisChannelSet = channelSets{ll,1};
                    thisChannelName = char(channelSets{ll,2});
                    
                    for mm = 1:nCepCoeffs
                        % Consider different numbers of cepstral
                        % coefficients
                        
                        thisCepCoeffs = cepCoeffs(mm);
                        
                        %Prints the variables:
                        thisSubject
                        thisChannelName
                        thisOverlap
                        thisFrameLength
                        thisCepCoeffs
                        
                        % Run the script:
                        run TRF_CC_algorithm
                        
                    end
                end                
            end
        end

    else
        warning('no reduced outfile exists for this subject');
        runFails = [runFails; thisSubject]
    end
end


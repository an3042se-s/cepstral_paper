
% Plots boxplots for each subject showing the classification probabilities

% NMSE_att, NMSE_unatt, r_att, and r_unatt contain all the info we need.
% NMSE_att for example contains the NMSE classifications for the 29 trials
% for that subject when the remaining trial was used to calculate the g.

% ISSUES:
% It should be noted that this analysis is based only on a few subjects -
% it was a preliminary test run! We will certainly need to run this again
% with all of the test subjects. Or can you run it on your pc with only a
% selected number of subjects? There were 8 subjects in this test run.

% For some reason there are only 29 trials for each subject. I don't know
% why this is. Ultimately I think we should re-run to get more results
% anyway so hopefully will not be a big problem.

clear all; clc;

%run set_directory_and_paths

addpath(genpath('E:\Autoruns_corrected'))
allFiles = dir( 'E:\Autoruns_corrected' );
allNames = { allFiles.name };

thisFile = allNames{4};
load(thisFile);
%%

% We have the workspaces save. They need to be loaded then the:
%   - Subject name
%   - Number of channels used
%   - Number of cepstral coeffs used
%   - Classification rate (NMSE)

store = {}
classification_rate = [];
subject = {};

for ijk = 3:length(allNames)
    thisFile = allNames{ijk};
    load(thisFile);
    ijk
    
    store{ijk-2,1} = thisSubject;
    store{ijk-2,2} = thisChannelName;
    store{ijk-2,3} = thisCepCoeffs;
    store{ijk-2,4} = thisFrameLength
    
    num_trials = length(trials);
    
    store{ijk-2,5} = sum(sum(NMSE_att > NMSE_unatt)/(thisCepCoeffs-1) > 0.5)/num_trials;
    
    clearvars -except allNames store classification_probabilities subject
end
    
%%
file_path = dir( 'C:\Users\Andrew\Desktop\segarmendozapaper\cepstral_paper\scripts\tests\classification_probabilities.mat' );
load(file_path.name);

file_path = dir( 'C:\Users\Andrew\Desktop\segarmendozapaper\cepstral_paper\scripts\tests\subject.mat' );
load(file_path.name);


%%
figure
boxplot(classification_rate,subject)
ylabel('NMSE');
xlabel('Subject');

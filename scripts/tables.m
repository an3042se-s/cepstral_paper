close; clc; clear

addpath(genpath('E:\Autoruns_corrected'))

allFiles = dir( 'E:\Autoruns_corrected' );
allNames = { allFiles.name };

%%
thisFile = allNames{3};
load(thisFile)

directory = 'E:\Thesis\Matlab\Auto\Autoruns\test_Welch_Set_9';

DataPath = [directory];
Tsubjects = dir(fullfile(DataPath));
Tsubjects = {Tsubjects(:).name}';

%Frames = {'frame25','frame50','frame75'};
Frames = {'frame25'};
%cep = {'cep6','cep8','cep11','cep14'};
cep = {'cep14'};

boxNMSEAttended = [];
boxNMSEUnattended = [];
boxRhoAttended = [];
boxRhoUnattended = [];

for ii = 1:length(Tsubjects)
%for ii = 2:4
    thisSubject = Tsubjects{ii}
    for jj = 1:length(Frames)
        thisFrame = Frames{jj}
        for kk = 1: length(cep)
            file = [];
            list = [];
            DataPath = [];
            thisCep = cep{kk}
            directory = 'F:\Thesis\Matlab\Auto\Autoruns\test_Welch_fullSet\test_welch';
            %DataPath = [directory '\' thisSubject '\channelsEarSet_3\' thisFrame '\ovlp0\' thisCep ];
            DataPath = [directory '\' thisSubject '\channelsFullSet\' thisFrame '\ovlp0\' thisCep ];
            list = dir(fullfile(DataPath, '*.mat'));
            file = list.name;
            load([DataPath '\' file])
            
            T =  struct2table(dataStore)
 
            boxNMSEAttended = [NMSE_att', boxNMSEAttended];
            boxNMSEUnattended = [NMSE_unatt', boxNMSEUnattended];
            
            boxRhoAttended = [r_att', boxRhoAttended];
            boxRhoUnattended = [r_unatt', boxRhoUnattended];
        end
    end
    %tables{ii}=T;
end
%% Scatter plots
x = 0.2:0.01:0.5;
figure
plot(x,x); hold on
for ii = 1:length(Tsubjects)
    scatter(boxNMSEAttended(:,ii),boxNMSEUnattended(:,ii),'b')
end
xlabel('NMSE_{Attended}')
ylabel('NMSE_{Unattended}')
hold off

median(median(boxNMSEAttended,1))
std(median(boxNMSEAttended,1))

median(median(boxNMSEUnattended,1))
std(median(boxNMSEUnattended,1))

x = 0.55:0.01:0.75;
figure
plot(x,x); hold on
for ii = 1:length(Tsubjects)
    scatter(boxRhoAttended(:,ii),boxRhoUnattended(:,ii),'b')
end
xlabel('\rho_{Attended}')
ylabel('\rho_{Unattended}')
hold off

median(median(boxRhoAttended,1))
std(median(boxRhoAttended,1))

median(median(boxRhoUnattended,1))
std(median(boxRhoUnattended,1))

%%
% Boxplots for NMSE
figure
boxplot(boxNMSEAttended,Tsubjects,'Notch',1)
ylabel('NMSE');
xlabel('Subject');

figure
set(boxplot(boxNMSEUnattended,Tsubjects), 'Color', 'r')
ylabel('NMSE');
xlabel('Subject');

figure
set(boxplot(boxNMSEAttended,Tsubjects,'Notch',1), 'Color', 'k'); hold on;
set(boxplot(boxNMSEUnattended,Tsubjects,'PlotStyle','compact'), 'Color', [0,0,0]+0.01); hold off
ylabel('NMSE'); ylim([0.2 0.55])
xlabel('Subject');

% Boxplots for Rho
figure
boxplot(boxRhoAttended,Tsubjects)
ylabel('\rho');
xlabel('Subject');

figure
set(boxplot(boxRhoUnattended,Tsubjects), 'Color', 'r')
ylabel('\rho');
xlabel('Subject');

figure
set(boxplot(boxRhoAttended,Tsubjects,'Notch',1), 'Color', 'k'); hold on;
set(boxplot(boxRhoUnattended,Tsubjects,'PlotStyle','compact'), 'Color', 'k'); hold off
ylabel('\rho'); ylim([0.55 0.75])
xlabel('Subject');

figure
subplot(211)
set(boxplot(boxNMSEAttended,Tsubjects), 'Color', 'b'); hold on;
set(boxplot(boxNMSEUnattended,Tsubjects), 'Color', 'r'); hold off
ylabel('NMSE'); ylim([0.2 0.55])
subplot(212)
set(boxplot(boxRhoAttended,Tsubjects), 'Color', 'b'); hold on;
set(boxplot(boxRhoUnattended,Tsubjects), 'Color', 'r'); hold off
ylabel('\rho'); ylim([0.55 0.75])
xlabel('Subject');


%%        
%writetable(T,'myTable.xls')
%writetable(T2,'myTable2.xls')

Frames = {'frame25','frame50','frame75'};
cep = {'cep6','cep8','cep11','cep14'};

myTableS1 = table2array(Table_set9_allFrames(:));

%subset = myTableS1(myTableS1(:,2)==25 & myTableS1(:,13)==14 , :)
subset = myTableS1(myTableS1.frameLength == 25 & myTableS1.cepCoeffs==14, :)
%NMSEClass = subset(:,14);
%RhoClass = subset(:,19);
NMSEClass = table2array(myTableS1(:,6));
RhoClass = table2array(myTableS1(:,11));

NMSEAttended = subset(:,15);
NMSEUnattended = subset(:,17);

RhoAttended = subset(:,20);
RhoUnattended = subset(:,22);

names = [Tsubjects];
figure % Classification based on NMSE and RHO
plot(NMSEClass, '-o'); 
set(0,'defaulttextInterpreter','latex')
set(gca,'xtick',[1:length(Tsubjects)],'xticklabel',names)
ylabel('Decoding Accuracy (%) based on NMSE'); ylim([50 100]); xlim([0 31])
xlabel('Subject')

figure
plot(RhoClass, '-o')
set(gca,'xtick',[1:length(Tsubjects)],'xticklabel',names)
ylabel('Decoding Accuracy (%) based on correlation'); ylim([50 100]); xlim([0 31])
xlabel('Subject')

%% Decoding accuracy based on rho using envelope
dec_acc_Env = [100, 100, 93, 90, 93, 73, 86, 96, 60, 100, 82.5, 96, 96, 92.5,...
    82.5, 90, 100, 100, 72.5, 82.5, 72.5, 92.5, 76, 96, 90, 86, 100, 96, 96, 90, 100];
mean(dec_acc_Env)
figure
plot(dec_acc_Env, '-o')
set(gca,'xtick',[1:length(Tsubjects)],'xticklabel',names)
ylabel('Decoding Accuracy (%) based on correlation'); ylim([50 100]); xlim([0 31])
xlabel('Subject')

%% all decoding accuracies in the same plot
names = [Tsubjects];
figure % Classification based on NMSE and RHO
plot(NMSEClass, '-o'); hold on
plot(RhoClass, '-*');
plot(dec_acc_Env, 'b--')
set(0,'defaulttextInterpreter','latex')
set(gca,'xtick',[1:length(Tsubjects)],'xticklabel',names)
legend('NMSE','\rho','\rho using envelope')
ylabel('Decoding Accuracy (%)'); ylim([50 100]); xlim([0 31])
xlabel('Subject')

%%
figure % NMSE and RHO
subplot(211);
plot(NMSEAttended, '-*'); hold on
plot(NMSEUnattended, '-*'); 
ylabel('NMSE')
subplot(212);
plot(RhoAttended, '-*'); hold on
plot(RhoUnattended, '-*'); 
ylabel('\rho')

%%
frame = 25; % 25, 50, 75
coeffs = 14; % 6,8,11,14
% Attended data
att = myTableS1.nmsemeanattended(myTableS1.frameLength==frame & myTableS1.cepCoeffs == coeffs);
unatt = myTableS1.nmseMeanUnattended(myTableS1.frameLength==25 & myTableS1.cepCoeffs == coeffs);
dataANOVA = [att unatt ];
% Unattended data
attRho = myTableS1.rhoMeanAttended(myTableS1.frameLength==frame & myTableS1.cepCoeffs == coeffs);
unattRho = myTableS1.rhoMeanUnattended(myTableS1.frameLength==25 & myTableS1.cepCoeffs == coeffs);
dataANOVA_Rho = [attRho unattRho];
group = {'Attended','Unattended'};

% t-test (test for equal means) May not work due to normality
% H0:=  X and Y comes from independent random samples from normal distributions
% with equal means and equal but unknown variances
% h = 0 => Do not reject H0
[h,p,ci,stats] = ttest2(att, unatt,'Vartype','unequal')
% Set9 attended
% p = 1.9927e-46
% h = 1 => Reject H0
[h,p,ci,stats] = ttest2(attRho, unattRho,'Vartype','unequal')
% Set9 unattended
% p = 5.6290e-59
% h = 1 => Reject H0

% Wilcoxon rank sum test (test for equal medians)
% H0:= X and Y are samples from continuous distributions with equal medians
% h = 1 => Reject H0
[p,h] = ranksum(att,att) 
[p,h] = signrank(att,att)
% Set9
% h = 0 => Do not reject H0
[p,h] = ranksum(attRho,attRho)
% h = 0 => Do not reject H0

% ANOVA
% H0:= 
p = anova1(dataANOVA,group); ylabel('NMSE')


p = anova1(dataANOVA_Rho, group); ylabel('\rho')

% Kruskal Wallis test (test distributions)
% H0:= Data in each column of the matrix A comes from the same distribution
% p<1% => Reject H0
[p_KW_NMSE, tbl,stats] = kruskalwallis(dataANOVA,group); ylabel('NMSE')
c = multcompare(stats)
% Set9
% p = 1.33e-11 < 1% => Reject H0
%Source      SS      df     MS      Chi-sq   Prob>Chi-sq
%-------------------------------------------------------
%Columns   14895.5    1   14895.5   45.76    1.33535e-11
%Error      4960     60      82.7                       
%Total     19855.5   61                                 

[p_KW_rho, tbl,stats] = kruskalwallis(dataANOVA_Rho,group); ylabel('\rho')
c = multcompare(stats)
% Set9
% p = 1.33e-11 < 1% => Reject H0
% Source      SS      df     MS      Chi-sq   Prob>Chi-sq
% -------------------------------------------------------
% Columns   14895.5    1   14895.5   45.76    1.33535e-11
% Error      4960     60      82.7                       
% Total     19855.5   61    
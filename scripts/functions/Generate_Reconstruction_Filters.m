function [g,R_lag] = Generate_Reconstruction_Filters_modified_R_dimension(R,S,lags,m)
%
% Generate stimulus reconstruction filters from neural responses
%
% S: Stimulus: time*frequency, the time-frequency representation of the training stimulus
% or simply time*1 (for an envelope)
% R: Response: channel*time, the corresponding neural responses to the training stimulus
% g: the reconstruction filters
% Lags: are the time delays of the stimulus used for the estimation. 
% E.g., for a sampling frequency of 64Hz, I recommend -19:0 (0 to ~300ms)
% Time-lags are reversed because this is a backwards mapping.
%
% Written by James O'Sullivan 07/28/15
% Lab of Nima Mesgarani

if size(R,2) > size(R,1) 
    R = R';
    if size(R,1)== 128 % electrodes
        R = R';
    end
end

if size(S,2) > size(S,1)
    S = S';
end

%R_lag = lagmatrix(R,lags);
R_lag = lagGenFrames(R,lags,m);
R_lag = [ones(size(R_lag,1),size(R,2)) R_lag];

for cc = 1:m
    tempR = R_lag(cc:m:end,:);
    tempS = S(cc:m:end);
    % remove NANs to zero - AS FM 2018-02-09
    k = find(isnan(tempR));
    tempR(k) = 0;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    RS = tempR'*tempS;
    RR = tempR'*tempR;

    [u,s,v] = svd(RR);
    D = diag(s);
    RR_Inv = D/sum(D);
    for cnt1 = 1:length(RR_Inv)
        if sum(RR_Inv(1:cnt1))>0.99,
            break;
        end
    end
    D = 1./D;
    D(cnt1+1:end) = 0;
    RR_Inv = (v*diag(D)*u');

    g(:,cc) = RR_Inv*RS;
end




function [c, X] = cepstrum(x, n, PSDE, K, NW, fs, window)
% [c, X] = cepstrum(x, n, PSDE, NFFT, K, NW, fs, window)
% cepstrum(x,n,'window', NFFT,~,~,fs,'hanning')
% x:= Nx1 vector with the signal
%
% n:= number of cepstral coefficients to display
%
% PSDE:= Power Spectral Density Estimate method:
%       periodogram:= periodogram(x, [], NFFT) -> default
%       hanning:= periodogram(x, hanning(N), NFFT)
%       welch:= pwelch(x, hanning(L), [], NFFT)
%               window length L = 2n/(K+1)            
%               L = floor(2*N/(K+1));
%       multitaper:= pmtm(x, NW, NFFT)
%
% NFFT:= Number of FFT points used to calculate the PSD estimate
%
% K:= Number of windows for Welch method
%
% NW:= "time-bandwidth product" for the discrete prolate spheroidal sequences
%      usually 2, 5/2, 3, 7/2, or 4

%% Set missing parameters to default values
N = length(x);

if ~exist('n','var')
 % # of cepstral coefficients to display
  n = 13;
end
if ~exist('PSDE','var')
 % Power spectral estimation method
  PSDE = 'periodogram';
end
if ~exist('K','var')
 % number of windows in the Welch method
  K = 30;
end
if ~exist('NW','var')
 % 'time-bandwidth product'
  NW = 2;
end

 if ~exist('window', 'var') || isempty(window)
     window = 'rectwin';
 end
 if ischar(window);
     window = eval(sprintf('%s(N)', window)); % hamming(N)
 end
 
 

%% Initialization
%{
if size(x,2)>size(x,1)
    x = x';
end
%}

pow = nextpow2(length(x));
a = 2^pow;
b = 2^(pow - 1);
threshold = b + (a - b)/2;

if length(x)<threshold
    NFFT = 2^(pow);
elseif length(x)>threshold
    NFFT = 2^(pow+1);
end


%x = x(:); % assure column vector

switch PSDE
    case 'fftWindow'
        [X] = fft(x.*window(:),NFFT);
    case 'periodogram' 
       [X, f] = periodogram(x,[],'twosided',NFFT,fs);
    case 'hanning'
        [X, f] = periodogram(x,hanning(N),'twosided',NFFT,fs);
    case 'welch'
        L = floor(2*N/(K+1));
        [X, f] = pwelch(x,hanning(L),[],NFFT,fs,'twosided');
    case 'multitaper'
        X = pmtm(x,NW,NFFT,fs,'twosided');
        
    otherwise
       [X, f] = periodogram(x,[],'twosided',NFFT,fs);
end

c = real(ifft(log(abs(X)+eps)));
c = c(2:n,:); % Drops the first cepstral coefficient
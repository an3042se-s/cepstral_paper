function out = lagGenFrames(R,lags,points)

out = zeros(size(R,1), size(R,2)*length(lags));
ind = 1;
% R(end+1:end+13,:) = 0;
for lag = lags
    t1 = circshift(R,lag*points);
    if lag < 0
        % t1(end-abs(lag):end,:) = 0;     % zero out end of time
        t1(end+lag*points+1:end,:) = 0;     % zero out end of time
    else
        t1(1:lag*points,:) = 0;                % Zero out first part of time.
    end
    out(:,ind:ind+size(R,2)-1) = t1(1:size(out,1),:);
    ind = ind + size(R,2);        % Advance to next lag slice
end

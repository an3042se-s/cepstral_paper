function out = Generate_Out_Reduced(DataPath,subject, StimPath, elecs, dataf, trials, prestim, poststim)

% Load parameters for auditory spectrogram
loadload;close;
dbstop if error
for t=trials
    tic
    disp(['Trial ' int2str(t)])
    name = fullfile(StimPath, ['JourneyL_20000R_run_' int2str(t) '.wav']);
    [audio,audiof] = audioread(name); 
    %  uiopen(name);
    
    % Find length of stimulus in EEG sample rate
    temp = resample(audio(:,1),dataf,audiof);
    L = length(temp) + prestim*dataf + poststim*dataf;
    
    % Load EEG Data 
    EEG = load([DataPath subject '_' int2str(t)]);   
    data = EEG.data; 
    triggers = EEG.triggers; 
    channelnames = EEG.channelnames;
    
    if size(data,2) > size(data,1)
        data = data';
    end
    
    % Find Onset
    onset = find(triggers == 200,1);

    % Add pre and post stimulus, and only keep selected electrodes 
    a = onset - prestim*dataf;
    b = onset - poststim*dataf + L;
    b = min(size(data,1),b);
    data = data(a : b ,elecs);
    channelnames = channelnames(elecs);
    
    audio = [zeros(prestim*audiof,2); audio; zeros(poststim*audiof,2)];
    
    %{
    % Get auditory spectrogram
    aud1 = wav2aud(audio(:,1), [1000/dataf 1000/dataf -2 log2(audiof/16000)] )';
    aud2 = wav2aud(audio(:,2), [1000/dataf 1000/dataf -2 log2(audiof/16000)] )';
    aud1 = aud1(:,1:L); % If size mismatch chop off end
    aud2 = aud2(:,1:L);
    
    % Get envelope
    env1 = mean(aud1);
    env2 = mean(aud2);
    
    % Hilbert Envelope
    tmp_aud1 = resample(audio(:,1),dataf*86,audiof); % Resample to be a multiple of dataf
    tmp_aud2 = resample(audio(:,2),dataf*86,audiof);
    envH1 = abs(hilbert(tmp_aud1));
    envH2 = abs(hilbert(tmp_aud2));
    envH1 = nt_dsample(envH1,86);
    envH2 = nt_dsample(envH2,86);
    %}
    
    % Assign to out structure
    out(t).audio=audio;
    out(t).audiof=audiof;
    out(t).dataf=dataf;
    out(t).data=data;
    out(t).channelnames=EEG.channelnames;
    %out(t).aud1=aud1;   
    %out(t).aud2=aud2;   
    %out(t).env1=env1;   
    %out(t).env2=env2;
    %out(t).envH1=envH1;   
    %out(t).envH2=envH2;     
    out(t).trial=t;
    
    clear audio
    toc
end

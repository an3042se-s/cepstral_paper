function [r1,r2,nmse1,nmse2] = Reconstruct_Stimulus(R,S1,S2,lags,g,m)
%
% Reconstruct a stimulus from neural responses
% 
% Inputs:
% S: Stimulus: time*frequency, the time-frequency representation of the test stimulus
% or simply time*1 (for an envelope)
% R: Response: channel*time, the corresponding neural responses to the test stimulus
% g: the reconstruction filters
% Lags: are the time delays of the stimulus used for the estimation. 
% E.g., for a sampling frequency of 64Hz, I recommend -19:0 (0 to ~300ms)
% Time-lags are reversed because this is a backwards mapping. 
%
% Outputs:
% r1: the correlation between the reconstructed stimulus and the 1st stimulus. 
% r2: the correlation between the reconstructed stimulus and the 2nd stimulus. 
%
% Written by James O'Sullivan 07/28/15
% Lab of Nima Mesgarani

if size(R,2) > size(R,1)
    R = R';
    if size(R,1)== 128 % electrodes
        R = R';
    end
end

if size(S1,2) > size(S1,1)
    S1 = S1';
end
if size(S2,2) > size(S2,1)
    S2 = S2';
end

R_lag = lagGenFrames(R,lags,m);
R_lag = [ones(size(R_lag,1),size(R,2)) R_lag];

r_stim = R_lag*g';


% remove NANs to zero - AS FM 2018-02-09
k = find(isnan(r_stim));
r_stim(k) = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

r1 = diag(corr(r_stim,S1));
r2 = diag(corr(r_stim,S2));

nmse1 = goodnessOfFit(r_stim,S1,'NMSE');
nmse2 = goodnessOfFit(r_stim,S2,'NMSE');




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOTE:
% This script needs to be called from the TRF_CC_master script.
% This is the TRF_CC algorithm that, with a set of established variables,
% will apply the TRF_CC method.
% This involves:
%   1) setting the EEG and speech signals so they have the same sampling
%     rate
%   2) finding the cepstral coefficients for the EEG and speech signals
%   3) applying the TRF method to these cepstral coefficients
%   4) quantifying the attention of the listener
%   5) saving the workspace for future reference if neccessary


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Variables
load('experimentInformation');
attention = 2-mod(strmatch(thisSubject, experimentInformation,'exact'),2);
                % Finds which speech stream was being attended to
                
eegFs = out(1).dataf(1);   % Sampling frequency of EEG data
speechFs = out(1).audiof(1);  % Sampling frequency of recording
nUpSamp = 20;           % Number of upsamples for EEG
adjustedFs = eegFs*nUpSamp;

trials = 1:30;          % Number of trials

% Lags are defined based of the assumption that the neural response to
% speech occurs between 0 and 450ms.
lags = -(450/thisFrameLength):0;

NFFT = 1024;            % for zero-padding

%% Calculate g vector for each trial

% Set g_all to be an empty matrix
g_all = [];


for t = trials
        
    disp(int2str(trials(t)))
    
    % Set empty matrices for saving Cepstral coefficients
    CCattended = [];
    CCunattended = [];
    CCEEG = {};
    
    % Set the EEG, attended and unattended speech
    eegRaw = out(t).data(:,thisChannelSet);
    if attention == 1
        attended = out(t).audio(:,1);
        unattended = out(t).audio(:,2);
    else
        attended = out(t).audio(:,2);
        unattended = out(t).audio(:,1);
    end
    
    % Downsample speech signals
    d = floor(speechFs/(eegFs*nUpSamp));
    attended = downsample(attended, d); 
    unattended = downsample(unattended, d); 
    
    attended = attended(1:size(attended,1),1);
    unattended = unattended(1:size(unattended,1),1);
    
    % Upsample EEG
    EEG =[];
    for i= 1:length(thisChannelSet)
     EEG(:,i) = interp(eegRaw(:,i),nUpSamp);
    end
    
    EEG = detrend(EEG,1);  % Type=1 removes linear trends
    EEG = detrend(EEG,0); % Type=0 removes signal means
    
    nsample = round((eegFs*nUpSamp)*thisFrameLength/1000); % number of samples in each frame
    noverlap = round((eegFs*nUpSamp)*(thisFrameLength*thisOverlap)/1000); % number of overlapped points

    % Setting equal lenghts 
    if length(EEG)>length(attended)
        EEG = EEG(1:length(attended),:);
    else
        attended = attended(1:length(EEG),1);
        unattended = unattended(1:length(EEG),1);
    end
    
    % Obtain Cepstral coefficients
    N = length(attended);
    pos = 1; i = 1;
    while (pos + nsample < N)
        attendedFrame = attended(pos : pos + nsample - 1);
        unattendedFrame = unattended(pos : pos + nsample - 1);
        eegFrame = EEG(pos : pos + nsample - 1,:);

        CCattended(:,i) = cepstrum(attendedFrame, thisCepCoeffs, spectralEstimationMethod, 10, 3, adjustedFs);
        CCunattended(:,i) = cepstrum(unattendedFrame, thisCepCoeffs, spectralEstimationMethod, 10, 3, adjustedFs);
        CCEEG(i) = {cepstrum(eegFrame, thisCepCoeffs, spectralEstimationMethod, 10, 3, adjustedFs)};

        pos = pos + (nsample - noverlap);
        i = i + 1;
    end
    
    % Reshape matrices with cepstral coefficients
    CCattended = reshape(CCattended,[],1);
    CCunattended = reshape(CCunattended,[],1);
    CCEEG = vertcat(CCEEG{:});
    
    % Generate filters
    [g,R] = Generate_Reconstruction_Filters(CCEEG,CCattended,lags,thisCepCoeffs-1); 
    
    % Save coefficients and filters for all trials
    CCattended_all(:,:,t) = {CCattended};
    CCunattended_all(:,:,t) = {CCunattended};
    CCEEG_all(:,:,t) = {CCEEG};
    
    g = reshape(g,[],1);
    g_all(:,:,t) = g;
end

%% Reconstruct Stimulus for each trial

for t = 1:trials(end)
    disp(['Trial ' int2str(t)])

    % Average filters from training data
    train = setdiff(1:length(trials),t);
    
    for cc = 1:(thisCepCoeffs-1)
        tempG = g_all(1+(cc-1)*size(R,2):cc*size(R,2),:,:);
        g_train = mean(tempG(:,train)');

        % ADD HERE TO CLASSIFY THE TEST SET IN STEPS RATHER THAN THE WHOLE
        % THING AT ONCE:
        % Use this to index:
        %       CCEEG_all{t}(1:10)
        
        mat = cell2mat(CCattended_all(:,:,t));
        tempCCa = mat(cc:(thisCepCoeffs-1):end,:);
        
        mat = cell2mat(CCunattended_all(:,:,t));
        tempCCu = mat(cc:(thisCepCoeffs-1):end,:);
        
        mat = CCEEG_all{t};
        tempCCEEG = mat(cc:(thisCepCoeffs-1):end,:);
        
        [r_att(cc,t), r_unatt(cc,t), NMSE_att(cc,t), NMSE_unatt(cc,t)] = ...
            Reconstruct_Stimulus(tempCCEEG, ...
            tempCCa, ...
            tempCCu,lags,g_train,thisCepCoeffs-1);
    end
end

%% Quantify Attention:
% NMSE

% Classifications:
classificationsNMSE = (sum(NMSE_att > NMSE_unatt)/(thisCepCoeffs-1)) > 0.5;

DA_NMSE = sum(classificationsNMSE)/length(trials)*100;


meanNmseAttendedSpeech = mean(NMSE_att);
stdNmseAttendedSpeech = std(NMSE_att);
meanNmseUnattendedSpeech = mean(NMSE_unatt);
stdNmseUnattendedSpeech = std(NMSE_unatt);


disp(['NMSE: ' num2str(DA_NMSE)])

% Pearsons Rho
% Perhaps using the absoulte value of the Pearsons R value is the way to go
% here.
classificationsRho = (sum(r_att > r_unatt)/(thisCepCoeffs-1)) > 0.5;

DA_rho = sum(classificationsRho)/length(trials)*100;

meanRhoAttendedSpeech = mean(r_att);
stdRhoAttendedSpeech = std(r_att);
meanRhoUnattendedSpeech = mean(r_unatt);
stdRhoUnattendedSpeech = std(r_unatt);


disp(['Rho: ' num2str(DA_rho)])


%% Save the workspace
% This uses a lot of space but saved us a lot of time when reviewing the
% results as it allowed us to try other methods in the 'Quantify attention'
% section without having to re-run the model.

variablesToSave = setdiff(who,{'out'});

proposedFileName = [SavePath ...     
            '\' thisSubject ...                     % Subject
            '\channels' char(channelSets{1,2}) ... % channel set
            '\frame' num2str(thisFrameLength) ...   % frame size
            '\ovlp' num2str(thisOverlap) ...    % overlap
            '\cep' num2str(thisCepCoeffs) ...    % number of cep coeffs
            '\Rho' num2str(DA_rho) ...        % Pearson's Rho result
            '_channels' thisChannelName '.mat'];
        
% Condition1: Does the cepstral folder exist?
condition1 = exist([SavePath ...
    '\' thisSubject ...
    '\channels' char(channelSets{1,2}) ...
    '\frame' num2str(thisFrameLength) ...
    '\ovlp' num2str(thisOverlap) ...
    '\cep' num2str(thisCepCoeffs)], 'dir') == 7;
% Condition2: Does the file already exist?
condition2 = exist(proposedFileName, 'file') == 2;
        

% Check existence of the folder that the workspace will be saved in. If it
% does not exist then create it:

if condition1 == true   % cepstral folder exists
    if condition2 == false % file of that name does not exist
        save('-v7.3', proposedFileName, variablesToSave{:});
    else % file of that name does exist
        warning('file already exists');
        fileAlreadyExisted = [fileAlreadyExists idx];
    end
else    % cepstral folder does not exist
    mkdir([SavePath '\' thisSubject ...
    '\channels' char(channelSets{1,2}) '\frame' num2str(thisFrameLength) ...
                '\ovlp' num2str(thisOverlap)], ['\cep' num2str(thisCepCoeffs)])
    save('-v7.3', proposedFileName, variablesToSave{:});
end

